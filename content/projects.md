+++
title = "Projects"
template = "page/default.html"
+++

## Kernel Patches

### asus-hid

Support all keyboard keys for laptops with 0x1866 device.

[[PATCH V4]: HID: asus: add support for ASUS N-Key keyboard](https://www.spinics.net/lists/linux-input/msg68440.html)

## Tools

### asus-nb-ctrl

Control of:

- AniMe matrix display
- Keyboard Aura backlight
- System profiles for FN+F5 (fan key)
- Setting battery charge limit

[https://gitlab.com/asus-linux/asus-nb-ctrl](https://gitlab.com/asus-linux/asus-nb-ctrl)
