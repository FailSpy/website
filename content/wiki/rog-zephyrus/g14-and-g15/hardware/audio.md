+++
title = "Audio"
description = "Audio adjustments"
sort_by = "none"
template = "page/wiki.html"
author = "Armas Spann"
+++

## Pulseaudio

To get the volume control working, it's needed to adjust "Master" and "PCM" for both analog-output-speaker and analog-output(common). To do so, adjust the following entry in `/usr/share/pulseaudio/alsa-mixer/paths/analog-output.conf.common`:

```text
[Element PCM]
switch = mute
volume = merge
override-map.1 = all
override-map.2 = all-left,all-right
```

As well as the following entries in `/usr/share/pulseaudio/alsa-mixer/paths/analog-output-speaker.conf`:

```text
[Element Hardware Master]
switch = mute
volume = ignore

[Element Master]
switch = mute
volume = ignore

[Element PCM]
switch = mute
volume = merge
override-map.1 = all
override-map.2 = all-left,all-right
```

Restart pulseaudio afterwards (as user: `pulseaudio -k`) and rerun any sound related application afterwards.

## Headset (Microphone) support

Is available for G14/15 in upstream kernel since 5.4/5.7/5.8 stable. Kernel patches not needed anymore
