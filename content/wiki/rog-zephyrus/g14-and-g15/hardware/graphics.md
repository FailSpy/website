+++
title = "Graphics"
description = "Howto setup the graphics"
sort_by = "none"
template = "page/wiki.html"
author = "Mateusz Schyboll"
co_author = "Armas Spann"
+++

## Blacklisting modules

put it into /etc/modprobe.d/, file name as you like

```text
blacklist nouveau
blacklist nvidiafb
blacklist rivafb
blacklist i2c_nvidia_gpu
```

`i2c_nvidia_gpu` causes some errors at boot, but system continues to work fine. But it also works fine without it.

## Module options

put it into /etc/modprobe.d/, file name as you like

```text
options nvidia_drm modeset=1
```

## Nvidia dGPU as default, main GPU

Manual Xorg config:

```text
Section "Module"
    Load "modesetting"
EndSection

Section "Device"
    Identifier "nvidia"
    Driver "nvidia"
    BusID "PCI:1:0:0"
    Option "AllowEmptyInitialConfiguration"
EndSection
```

Note, this configs by default doesn't display anything on the build in display, only via the USB-C alternate DP output.
You need to put into the autostart of you DE or login manager `xrandr --setprovideroutputsource modesetting NVIDIA-0; xrandr --auto`. If any one knows a better solution for getting this done in xorg config fell free to update that information.

## iGPU as default, main GPU. Nvidia by using prime

Xorg config:

```text
Section "Device"
  Identifier "iGPU"
  Driver "amdgpu"
EndSection

Section "Screen"
  Identifier "iGPU"
  Device "iGPU"
EndSection

Section "Device"
  Identifier "dGPU"
  Driver "nvidia"
  Option "AllowEmptyInitialConfiguration"
  Option "NVreg_DynamicPowerManagement=0x02"
EndSection


#On Arch this is added automatic by the nvidia-prime package
Section "ServerLayout"
    Identifier "layout"
    Option "AllowNVIDIAGPUScreens"
EndSection
```

For arch you can use [nvidia-prime](https://www.archlinux.org/packages/extra/any/nvidia-prime/) and run any program with the prefix `prime-run`. For others just create a script:

```bash
#!/bin/bash
__NV_PRIME_RENDER_OFFLOAD=1 __VK_LAYER_NV_optimus=NVIDIA_only __GLX_VENDOR_LIBRARY_NAME=nvidia "$@"
```

## Power management

put it into /etc/modprobe.d/, file name as you like

```text
options nvidia NVreg_DynamicPowerManagement=0x02
```

put it into /etc/udev/rules.d/, file name as you like. This can disable the USB-C connector on the left so you can try without it.

```text
# Remove NVIDIA USB xHCI Host Controller devices, if present
ACTION=="add", SUBSYSTEM=="pci", ATTR{vendor}=="0x10de", ATTR{class}=="0x0c0330", ATTR{remove}="1"

# Remove NVIDIA USB Type-C UCSI devices, if present
ACTION=="add", SUBSYSTEM=="pci", ATTR{vendor}=="0x10de", ATTR{class}=="0x0c8000", ATTR{remove}="1"

# Remove NVIDIA Audio devices, if present
ACTION=="add", SUBSYSTEM=="pci", ATTR{vendor}=="0x10de", ATTR{class}=="0x040300", ATTR{remove}="1"

# Enable runtime PM for NVIDIA VGA/3D controller devices on driver bind
ACTION=="bind", SUBSYSTEM=="pci", ATTR{vendor}=="0x10de", ATTR{class}=="0x030000", TEST=="power/control", ATTR{power/control}="auto"
ACTION=="bind", SUBSYSTEM=="pci", ATTR{vendor}=="0x10de", ATTR{class}=="0x030200", TEST=="power/control", ATTR{power/control}="auto"

# Disable runtime PM for NVIDIA VGA/3D controller devices on driver unbind
ACTION=="unbind", SUBSYSTEM=="pci", ATTR{vendor}=="0x10de", ATTR{class}=="0x030000", TEST=="power/control", ATTR{power/control}="on"
ACTION=="unbind", SUBSYSTEM=="pci", ATTR{vendor}=="0x10de", ATTR{class}=="0x030200", TEST=="power/control", ATTR{power/control}="on"
```

Note is is not possible to disable the dGPU complete (for now), it goes into P8 state and uses from 1 to 3W (1660Ti uses more power).
Also good option is to put into you DE autostart `nvidia-settings -a "[gpu:0]/GpuPowerMizerMode=0"`

## iGPU as default, main GPU. Offloading iGPU to dGPU for using the USB-C alternate DP output (aka reverse PRIME)

Not possible for now, but maybe the 450.XX driver will change something in this solution, no tested yet.
